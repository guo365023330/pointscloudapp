// swiftlint:disable all
import Amplify
import Foundation

public struct Todo: Model {
  public let id: String
  public var name: String
  public var description: String?
  public var points: Double
  public var historyArray: [String]
  
  public init(id: String,
      name: String,
      description: String? = nil,
      points: Double,
      historyArray: [String] = []) {
      self.id = id
      self.name = name
      self.description = description
      self.points = points
      self.historyArray = historyArray
  }
}
