//
//  OutputInterfaceUI.swift
//  Simple App
//
//  Created by Guo on 2020/10/23.
//  Copyright © 2020 Guo. All rights reserved.
//

import SwiftUI
import Amplify

struct OutputInterfaceUI: View {
//    var searchField : UITextField
//    var outputView: UIView
//    var rect: CGRect!
    @State var userId : String
    @State var pointsStr : String
    @State var address: String = ""
    var body: some View {
        //NavigationView{
            VStack{
                TextField( "输入地址", text: $address)
                .frame(width: 300, height: 200, alignment: .bottom)
                .textFieldStyle(RoundedBorderTextFieldStyle())
                .background(Color.yellow)
                Button(action: getInfo) {Text("识别")
                }
                
                TextField("USER Id", text: $userId)
                    .keyboardType(.numberPad)
                    .textFieldStyle(RoundedBorderTextFieldStyle())
                    .padding(15)
                    .padding(.horizontal)
                TextField("Points", text: $pointsStr)
                    .keyboardType(.numberPad)
                    .textFieldStyle(RoundedBorderTextFieldStyle())
                    .padding(15)
                    .padding(.horizontal)
                Button(action: getInfo) {
                    ZStack{
                        Circle()
                        .frame(width: 80, height: 80)
                        .foregroundColor(.yellow)
                      Text("查询")
                          .foregroundColor(.green)
                    }
                }
            }
       // }
    }
    
    func getInfo() {
        let id = userId
        Amplify.API.query(request: .get(Todo.self, byId: id)) { event in
            switch event {
            case .success(let result):
                switch result {
                case .success(let todo):
                    guard let todo = todo
                        else {
                        print("Could not find todo")
                        return
                    }
                    DispatchQueue.main.async{
                        self.pointsStr = todo.points.description
//                        let vc = ResultPopView(imageUrl: <#String#>).setupView(str : " ")
//                        vc.modalPresentationStyle = UIModalPresentationStyle.automatic
//                        
//                        let popVC = vc.popoverPresentationController
//                        present
//                        popVC?.delegate = self.
//                        
//                        self
//                        
//                        
                    }
                case .failure(let error):
                    print("Got failed result with \(error.errorDescription)")
                }
            case .failure(let error):
                print("Got failed event with error \(error)")
            }
        }
    }
}

struct OutputInterfaceUI_Previews: PreviewProvider {
    static var previews: some View {
        OutputInterfaceUI(userId: "", pointsStr: "")
    }
}
