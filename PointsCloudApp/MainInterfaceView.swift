//
//  MainInterfaceView.swift
//  Simple App
//
//  Created by Guo on 2020/10/14.
//  Copyright © 2020 Guo. All rights reserved.
//

import Foundation
import UIKit
import SwiftUI

class MainInterfaceView: UIViewController, UITabBarDelegate{
    //var outputStruct = OutputInterfaceView()
    @IBOutlet weak var inputTabButton: UITabBarItem!
    @IBOutlet weak var outputTabButton: UITabBarItem!
    @IBOutlet weak var tabBar: UITabBar!
    var scrollView : UIScrollView!
    
    var rect : CGRect!
    var inputInterface : UIView!
    var outputInterface : UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tabBar.delegate = self
//      let backToMain = UIBarButtonItem(title: "主页", style: .plain, target:self, action: #selector(barbuttonClick(button:)))
//      self.navigationItem.leftBarButtonItem = backToMain
        rect = self.view.bounds
        rect.origin.y = NAVANDSTATUS_HEIGHT
        rect.size.height = rect.size.height - NAVANDSTATUS_HEIGHT - self.tabBar.frame.height
        self.scrollView = UIScrollView(frame: rect)
        self.view.addSubview(scrollView)
        rect.origin.y = 0
        inputInterface = InputInterfaceView(frame: rect)
        let vc: UIHostingController = UIHostingController(rootView: OutputInterfaceUI(userId: " ", pointsStr: " "))//调用swiftui的方法
        vc.view.frame = rect
        outputInterface = vc.view
//        outputInterface = OutputInterfaceView(rect: rect).outputView
        self.scrollView.addSubview(outputInterface)
        self.scrollView.addSubview(inputInterface)
    }
    
    
    //代理方法
    func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        if (item == inputTabButton) {
            outputInterface.removeFromSuperview()
            self.scrollView.addSubview(inputInterface)
        }else{
            inputInterface.removeFromSuperview()
            self.scrollView.addSubview(outputInterface)
        }
    }
    
    
    
  

//    }
}


