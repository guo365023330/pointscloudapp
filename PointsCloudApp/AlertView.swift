//
//  AlertView.swift
//  PointsCloudApp
//
//  Created by Guo on 2020/12/03.
//  Copyright © 2020 Guo. All rights reserved.
//

import Foundation
import UIKit

class AlertView{
    
    func initALert(str:String) -> UIAlertController {
        let alert = UIAlertController(title: "警告", message: str, preferredStyle: .alert)
        let action = UIAlertAction(title: "确定", style: .default, handler: nil)
        alert.addAction(action)
        return alert
    }
    
    func getCurrentViewController() -> UIViewController? {

        if let rootController = UIApplication.shared.windows.filter {$0.isKeyWindow}.first?.rootViewController{
            var currentController: UIViewController! = rootController
            while( currentController.presentedViewController != nil ) {
                currentController = currentController.presentedViewController
            }
            return currentController
        }
        return nil

    }
}
