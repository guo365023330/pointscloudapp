//
//  InputInterfaceView.swift
//  Simple App
//
//  Created by Guo on 2020/10/12.
//  Copyright © 2020 Guo. All rights reserved.
//

import Foundation
import UIKit
import Amplify

class InputInterfaceView: UIView, UITextFieldDelegate, UITextViewDelegate, UIPopoverPresentationControllerDelegate{//和结构体不同的是，类必须自己声明初始化构造器：
    private var contentView    : UIView!
    private var responseView   : UITextField? //当前选中的输入框
    private var selectedView   : UITextView? //当前选中的输入框
    private var itemData       : Todo?
    private var todoExist      : String?
    private var resultStr      : String?

    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var analyzeBtn: UIButton!
    @IBOutlet weak var nameField: UITextField!
    @IBOutlet weak var telField: UITextField!
    @IBOutlet weak var amountField: UITextField!
    @IBOutlet weak var submitBtn: UIButton!
    @IBOutlet weak var expensiveItem: UITextField!
    @IBOutlet weak var usePoints: UITextField!
    @IBOutlet weak var isDiscount: UISwitch!
    override init(frame: CGRect) {
        super.init(frame: frame)    //???
        contentView = loadNib()
        addSubview(contentView)
        self.contentView.backgroundColor = UIColor.yellow
    }
    
    required init?(coder aDecoder: NSCoder) {  //??? 必须加
       super.init(coder:aDecoder)
    }
    
    override func awakeFromNib() {
        //view基于xib的初始化不经过这个方法的话xib的对象属性都是空
        self.textView.layer.borderWidth = 1
        self.textView.layer.cornerRadius = 6.0;
        self.textView.layer.borderColor = UIColor.gray.cgColor //cgcolor的使用
        super.awakeFromNib()
        //输入部分添加textfield/textview代理
        textView.delegate      = self
        nameField.delegate     = self
        telField.delegate      = self
        amountField.delegate   = self
        expensiveItem.delegate = self
        usePoints.delegate     = self
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyBoardWillShow(notification:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyBoardWillHide(notification:)), name: UIResponder.keyboardWillHideNotification,object: nil)
    }
    
    //系统代理方法，开始输入时
    func textFieldDidBeginEditing(_ textField: UITextField) {
        responseView = textField
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        selectedView = textView
    }

    // MARK: - 键盘即将弹出
    @objc func keyBoardWillShow(notification: Notification) {
        RunLoop.current.run(mode: .default, before: NSDate(timeIntervalSinceReferenceDate: 0.3) as Date)
        if responseView == nil{
            return
        }else{
            let userInfo = notification.userInfo! as Dictionary
            let value = userInfo[UIResponder.keyboardFrameEndUserInfoKey] as! NSValue
            let keyBoardRect = value.cgRectValue
            // 得到键盘高度
            let rect =  self.responseView!.convert(self.responseView!.bounds, to: self.superview)
            let offsetY = UIScreen.main.bounds.height - rect.origin.y - rect.size.height - keyBoardRect.origin.y
            if (responseView != nil) && (offsetY < 0) {
              // scrollView现在的偏移量 + 需要移动的偏移量 = 最终要设置的偏移量
                self.frame.origin.y += offsetY
            }
            // 如果要移动距离大于0说明有遮盖，说明需要移动
        }
    }

    // MARK: - 键盘即将回收
    @objc func keyBoardWillHide(notification: Notification) {
        selectedView = nil
        responseView = nil
        self.frame.origin.y = 0
    }
    
//    // キーボードを閉じる1
//    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
//        textField.resignFirstResponder()
//        return true
//    }
    
    // キーボードを閉じる2
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.endEditing(true)
    }
    
    func popUpMessage() {
        let currentVC = AlertView().getCurrentViewController()
        //程序会先走到这，再去block，所以要等
        while (self.resultStr == nil) {
            RunLoop.current.run(mode: .default, before: NSDate(timeIntervalSinceReferenceDate: 0.3) as Date)
        }
        if self.resultStr == "查询数据出错" {
            let alertView = AlertView().initALert(str: "数据操作没执行")
            currentVC?.present(alertView, animated: true, completion: nil)
        }else{
            let alertView = AlertView().initALert(str: self.resultStr!)
            currentVC?.present(alertView, animated: true, completion: nil)
        }
    }
    
    func getPoints(text: String) -> Double{
        var resultP : Double = 0
        if  Double(text) == 0 {
            return 0
        }
        let totalBuy = Double(text)! - 10.0
        if isDiscount.isOn {
            resultP = totalBuy*0.02
        }else{
            if expensiveItem.text != "0"{
                let expen = Double(expensiveItem.text!)!
                let resultP = (totalBuy + 10 - expen)*0.05 + expen*0.02 - 0.5
                return roundDouble(data: resultP)
            }
            resultP = totalBuy*0.05
        }
        return roundDouble(data: resultP)
    }
    
    func roundDouble(data: Double) -> Double {
        let tempData = data*100
        return Double(round(tempData)/100)
    }
    
    func loadNib()->UIView{
        //return Bundle.main.loadNibNamed("InputInterfaceView", owner: self, options: nil)?.first as! UIView
        let nib = UINib(nibName: "InputInterfaceView", bundle: .main)
        let view = nib.instantiate(withOwner: self, options: nil).first as! UIView
        return view
    }
    
    @IBAction func analizeClick(_ sender: Any) {
        let currentVC = AlertView().getCurrentViewController()
        if textView.text == nil {
            let alertView = AlertView().initALert(str: "请输入地址")
            currentVC?.present(alertView, animated: true, completion: nil)
        }else if (!textView.text.contains("1")){
            let alertView = AlertView().initALert(str: "地址里没有电话号码")
            currentVC?.present(alertView, animated: true, completion: nil)
        }else{
            telField.text = AnalyzeUserInfo().analyzeStr(userInfoStr:textView.text)
        }
    }
    
    @IBAction func submitClick(_ sender: Any) {
        checkIdExist()
        while (todoExist == nil) {
            RunLoop.current.run(mode: .default, before: NSDate(timeIntervalSinceReferenceDate: 0.3) as Date)
        }
        var pointsChangeStr: String = ""
        var historyArray   : [String]!=Array()
        var historyStr : String = ""
        let date = Date()
        let calendar = Calendar.current
        let year  = calendar.component(.year, from: date)
        let month = calendar.component(.month, from: date)
        let day   = calendar.component(.day, from: date)
        let buyDate = String(year) + String(month) + String(day)
        let points = getPoints(text: self.amountField.text ?? "0")
        if (isDiscount.isOn) || (expensiveItem.text != "0") {
              historyStr = buyDate + " $" + (amountField.text ?? "0")
        }else{
              historyStr = buyDate + " " + (amountField.text ?? "0")
        }
        historyStr = historyStr + " " + String(points)
        historyArray.append(historyStr)
        if todoExist == "YES" {
        //更新数据
            Amplify.API.query(request: .get(Todo.self, byId: self.telField.text!)) {
               event in
                  switch event {
                  case .success(let result):
                      switch result {
                      case .success(let todo):
                          if todo == nil {
                              print("Could not find todo")
                              self.resultStr = "未查询到该用户"
                              return
                          }else{
                              DispatchQueue.main.async{
                                  self.itemData = todo
                              }
                          }
                      case .failure(let error):
                          print("Got failed result with \(error.errorDescription)")
                          self.resultStr = "查询数据失败"
                          return
                      }
                  case .failure(let error):
                      print("Got failed event with error \(error)")
                      self.resultStr = "查询数据失败（API）"
                      return
                  }
            }
            while (self.itemData == nil) {
               RunLoop.current.run(mode: .default, before: NSDate(timeIntervalSinceReferenceDate: 0.3) as Date)
            }
            self.itemData!.historyArray.append(historyStr)
            pointsChangeStr = String(points)
            self.itemData!.points += points
            self.itemData!.points = roundDouble(data: self.itemData!.points)//删掉精度小数点
            pointsChangeStr = String(self.itemData!.points) + " " + pointsChangeStr

            //变更开始
            Amplify.API.mutate(request: .update(self.itemData!)) { event in
                switch event {
                case .success(let result):
                    switch result {
                    case .success(let todo):do {
                        print("Successfully updated the todo: \(todo)")
                        self.resultStr = "成功更新数据，积分: " + pointsChangeStr
                        }
                    case .failure(let graphQLError):
                        print("Failed to update graphql \(graphQLError)")
                        self.resultStr = "更新数据失败(更新出错)"
                   }
                case .failure(let apiError):
                    print("Failed to update a todo", apiError)
                    self.resultStr = "更新数据失败(api出错)"
                }
            }
        //不存在
        }else if todoExist == "NO" {
            //新建todo
            let todo = Todo(id:self.telField.text!, name: nameField.text!, description: "userdata", points: points, historyArray: historyArray!)
            Amplify.API.mutate(request: .create(todo)) { event in
                switch event {
                case .success(let result):
                    switch result {
                    case .success(let todo):do {
                        print("Successfully created the todo: \(todo)")
                        self.resultStr = "新建数据成功 " + todo.id + " " + todo.points.description}
                    case .failure(let graphQLError):
                        print("Failed to create graphql \(graphQLError)")
                        self.resultStr = "新建数据失败(更新出错)"
                    }
                case .failure(let apiError):
                    print("Failed to create a todo", apiError)
                    self.resultStr = "新建数据失败(api出错)"
                }
            }
        }else{
            self.resultStr = "查询数据出错"
        }
        popUpMessage()
        todoExist = nil
        self.itemData = nil
        self.resultStr = nil
    }

    func checkIdExist() {
        Amplify.API.query(request: .get(Todo.self, byId: telField.text ?? "0")) { event in
            switch event {
            case .success(let result):
                switch result {
                case .success(let todo):
                    if todo == nil {
                        print("todo is empty")
                        self.todoExist = "NO"
                        return
                    }else{
                        self.todoExist = "YES"
                        print("todo exsit")
                    }
                case .failure(let error):
                    print("Got failed result with \(error.errorDescription)")
                    self.todoExist = "Failure"
                }
            case .failure(let error):
                print("Got failed event with error \(error)")
                self.todoExist = "Failure"
            }
        }
    }
}

