//
//  WebViewController.swift
//  Simple App
//
//  Created by Guo on 2020/07/28.
//  Copyright © 2020 Guo. All rights reserved.
//

import UIKit
import WebKit


class WebViewController: UIViewController {
    var webView         : WKWebView!
    var configuration   : WKWebViewConfiguration!
    var urlTextField    : UITextField!
    //var countLabel      : UILabel!
    var backwardBtn     : UIBarButtonItem!
    var forewardBtn     : UIBarButtonItem!
    var pageCountLable  : UILabel!
    var arrayBtn        : Array<UIBarButtonItem> = [] //重点
    var canGoBack       : Bool = true
    var canGoForward    : Bool = true
    override func viewDidLoad() {
        super.viewDidLoad()
        initWebView()
        createBtn()
        self.view.addSubview(webView)
        // Do any additional setup after loading the view.
    }
    
    func initWebView() {
        
        configuration = WKWebViewConfiguration()
        //允许视频播放
        configuration.allowsAirPlayForMediaPlayback = true
        // 允许在线播放
        configuration.allowsInlineMediaPlayback = true
        // 允许可以与网页交互，选择视图
        configuration.selectionGranularity = WKSelectionGranularity(rawValue: 10)!
        //默认字体大小
        configuration.preferences.minimumFontSize = 9.0
        //添加监控事件
        configuration.userContentController = WKUserContentController()
        
        webView = WKWebView(frame: CGRect(x: 0, y: 3, width: SCREEN_WIDTH, height: SCREEN_HEIGHT - NAVANDSTATUS_HEIGHT - TOOLBAR_HEIGHT ), configuration: configuration)
        webView.backgroundColor = UIColor.yellow
    }
    
    func createBtn(){
        self.navigationController?.isToolbarHidden = false
        //底部工具栏-退回
        backwardBtn = UIBarButtonItem(image: UIImage(named:"backward.png"), style:.plain, target: self, action:#selector(barbuttonClick(button:)))
        backwardBtn.tag = 100;
        
        backwardBtn.setBackgroundImage(UIImage(named:"backward-gray.png"), for: .disabled, barMetrics:.default)
       // backwardBtn.isEnabled = false
        
        //底部工具栏-主页
        let btnMain = UIBarButtonItem(title: "主页", style: .plain, target: self, action: #selector(barbuttonClick(button:)))
        btnMain.tag = 101;
        
        //底部工具栏-前进
        forewardBtn = UIBarButtonItem(image: UIImage(named:"foreward.png"), style:.plain, target: self, action:#selector(barbuttonClick(button:)))
        forewardBtn.tag = 102;
               
        forewardBtn.setBackgroundImage(UIImage(named:"foreward-gray.png"), for: .disabled, barMetrics:.default)
        forewardBtn.isEnabled = false
      
         
        //固定宽度占位按钮
        let btnF01 = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: self, action: nil)//自动计算间隙
        
        // JS交互的按钮
        let jsBtn1 = UIBarButtonItem(title: "2js无参", style: .plain, target: self, action: #selector(barbuttonClick(button:)))
        jsBtn1.tag = 300;
        
        let jsBtn2 = UIBarButtonItem.init(title: "2js有参", style: .plain, target: self, action: #selector(barbuttonClick(button:)))
        jsBtn2.tag = 301;
        
        //self.navigationController?.setToolbarItems(arrayBtn, animated: false)
        //self.navigationController?.toolbar.setItems(arrayBtn, animated: true)
        self.toolbarItems = [backwardBtn, btnF01,jsBtn1,btnF01,btnMain,btnF01,jsBtn2, btnF01, forewardBtn]
        self.navigationController?.setToolbarHidden(false, animated: false)
        
        //导航栏按钮
        let backToMain = UIBarButtonItem(title: "主页", style: .plain, target:self, action: #selector(barbuttonClick(button:)))
        self.navigationItem.leftBarButtonItem = backToMain
        backToMain.tag = 200
        
        //更多选项
        let rightBtn = UIBarButtonItem(barButtonSystemItem: .add, target:self, action: #selector(barbuttonClick(button:)))
        self.navigationItem.rightBarButtonItem = rightBtn
        rightBtn.tag = 201;
        
        //网页数量countLable
        let rectView = CGRect(x: 0.8*SCREEN_WIDTH, y: NAVIBAR_HEIGHT/2 - 14, width: 25, height: NAVIBAR_HEIGHT/2)
        pageCountLable = UILabel(frame: rectView)
        
        pageCountLable.backgroundColor = UIColor.yellow
        pageCountLable.textAlignment = NSTextAlignment(.center)
        pageCountLable.textColor = UIColor.gray
        pageCountLable.layer.borderWidth = 2;
        pageCountLable.layer.borderColor = UIColor.gray.cgColor
        self.navigationController?.navigationBar.addSubview(pageCountLable)
        
        //网址输入栏
        let textFiled = UITextField(frame: CGRect(x: 65, y: NAVIBAR_HEIGHT/2 - 14, width: SCREEN_WIDTH - 150, height: NAVIBAR_HEIGHT/2 + 6))
           
        textFiled.borderStyle = .roundedRect
        textFiled.layer.cornerRadius = 20;
        self.navigationController?.navigationBar.addSubview(textFiled)
    }

    @objc func barbuttonClick (button: UIBarButtonItem){
        switch (button.tag) {
            case 100:
                if (canGoBack) {
                   // WKBackForwardListItem.
                    webView.goBack()
                    backwardBtn.isEnabled = canGoBack;
                    forewardBtn.isEnabled = canGoForward;
                    // WKBackForwardListItem.
                }
                break;
            case 101:
                //加载本地服务器html文件
                #if targetEnvironment(simulator)
                    guard let filePath = Bundle.main.path(forResource: "ab", ofType: "html") else { return }
                    let baseUrl =  Bundle.main.bundleURL
                    do {
                        let htmlStr = try String(contentsOfFile: filePath)
                        webView.loadHTMLString(htmlStr, baseURL: baseUrl)
                    } catch {
                        webView.loadHTMLString(error.localizedDescription, baseURL: baseUrl)// must Handle the error
                    }
              
                    //加载内嵌html文件
                #else
                    let baseUrl = URL(string: "http://127.0.0.1/ab.html")
                    let baseRequest = NSURLRequest(url: baseUrl)
                    webView.load(baseRequest)
                #endif
              
//                for view in webView {
//                    if([view isKindOfClass:[UrlCollectionView class]]){
//                        [view removeFromSuperview];
//                    }
//                }
            
                break;
            case 102:
                if (webView.canGoForward) {
                    webView.goForward()
                    backwardBtn.isEnabled = webView.canGoBack;
                    forewardBtn.isEnabled = webView.canGoForward;
                }
                break;
            case 200:
                //返回登陆界面
                //self.backToMainPage
                break;
              /*
            case 201:
                /** 显示下拉菜单 */
                //创建实例
                //示例调用
                @autoreleasepool {
                    DropDownMenuView *menuView = [[DropDownMenuView alloc] init];
                    [menuView setupDropDownMenu];
                    //关闭键盘
                    _isKeyBoardShow = NO;
                    [_currentWebView endEditing:YES];
                    while (_isKeyBoardShow == NO) {
                        [[NSRunLoop currentRunLoop] runUntilDate:[NSDate dateWithTimeIntervalSinceNow:0.1]]; //卡线程
                    }
                    NSString *elementName = [self getActiveELement];
                    if (elementName != nil) {
                        [_currentWebView evaluateJavaScript:@"document.activeElement.blur()" completionHandler:nil];
                    }
                    //block传值-实现block接收传值
                    self.menuTag = -1;
                    menuView.passRowNumBlock = ^(NSInteger menuTag){
                        self.menuTag = menuTag;
                        switch (self.menuTag) {
                            case 0:
                                //新开网页
                                [self makeWKWebView];
                                self.pageCount += 1;
                                self.pageCountLable.text = [NSString stringWithFormat:@"%ld", self.pageCount];
                                break;
                            case 1:
                                //刷新网页
                                self.url = [NSURL URLWithString:self.textFiled.text];
                                [self loadWebView:self.url];
                                break;
                            case 2:
                                {
                                    //关闭当前网页
                                    self.currentWebView .navigationDelegate = nil;
                                    self.currentWebView .UIDelegate = nil;
                                    [self.currentWebView stopLoading];
                                    [self.currentWebView removeFromSuperview];
                                    [self.WebViewArray removeObject:self.currentWebView];
                                    self.pageCount -=1;
                                    self.pageCountLable.text = [NSString stringWithFormat:@"%ld", self.pageCount];
                                    self.currentWebView = [self.WebViewArray objectAtIndex:self.pageCount];
                                    self.url = [NSURL URLWithString:self.textFiled.text];
                                    //self.currentWebView = self.WebViewArray
                                    [self.currentWebView reload];
                                    break;
                                }
                            case 3:
                            //QRCode读取
                                {
                                    CodeScanView *scanView = [[CodeScanView alloc] initWithFrame:self.webPageRect];
                                    [self.currentWebView addSubview:scanView];
                                    scanView.backQRCodeURL = ^(NSString *stringValue){
                                        self.url = [NSURL URLWithString:stringValue];
                                        [self loadWebView:(self.url)];
                                        for (UIView *view in self.currentWebView.subviews) {
                                            if([view isKindOfClass:[CodeScanView class]]){
                                                [view removeFromSuperview];
                                            }
                                        }
                                    };
                                }
                            break;
                            case 4:
                            //主页标签
                                [self loadBookMark];
                                break;
                            case 5:
                            //購買登録
                                {
                                    UserInfoController *userView = [[UserInfoController alloc]initWithNibName:@"UserInfoController" bundle:[NSBundle mainBundle]];
                                    [self.navigationController pushViewController:userView animated:YES];
                                }
                            break;
                            default:
                                break;
                        }};
                }
            case 300:
                //在这里返回给h5信息
                [_currentWebView evaluateJavaScript:@"transferJSAction()" completionHandler:nil];
                break;
            case 301:
                //在这里返回给h5信息
                //信息的格式可以是任何形式的，这里用字符串
                {NSString * str = @"OC传递给h5的内容";
                NSString * sendMessage = [NSString stringWithFormat:@"transferJSActionAndMessage('%@')",str];
                [_currentWebView evaluateJavaScript:sendMessage completionHandler:^(id _Nullable response, NSError * _Nullable error) {
                    NSLog(@"error %@", error);//可以打印错误信息
                }];
                }
                break;
            */
            default:
                break;
        }
    }
 
    func backToMainPage() {
        //let view = UIView()
//        for UIView() in self.navigationController?.navigationBar.subviews{
//            [view removeFromSuperview];
    //  }//回到主页前移除subview
//        self.navigationController?.setToolbarHidden(true, animated: false) //回到主页时底部隐藏toobar
//        self.navigationController?.popViewController(animated: true)
//        self.navigationController?.navigationBar.barTintColor = UIColor.white
    }
}
