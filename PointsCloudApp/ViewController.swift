//
//  ViewController.swift
//  PointsCloudApp
//
//  Created by Guo on 2020/09/23.
//  Copyright © 2020 Guo. All rights reserved.
//

import UIKit
import Amplify
import AmplifyPlugins

//获取屏幕宽度、高度
public let SCREEN_WIDTH = UIScreen.main.bounds.size.width
public let SCREEN_HEIGHT = UIScreen.main.bounds.size.height
// 状态栏高度
public let window = UIApplication.shared.windows.filter {$0.isKeyWindow}.first
public let STATUSBAR_HEIGHT : CGFloat! =  window?.windowScene?.statusBarManager?.statusBarFrame.size.height
// 导航栏高度
public let  NAVIBAR_HEIGHT = UINavigationController().navigationBar.frame.size.height
//获取导航栏+状态栏的高度
public let  NAVANDSTATUS_HEIGHT  = NAVIBAR_HEIGHT + STATUSBAR_HEIGHT
// 工具栏高度
public let TOOLBAR_HEIGHT = UINavigationController().toolbar.frame.size.height


class ViewController: UIViewController {
    var pwLb     : UILabel!
    var userTf   : UITextField!
    var pwTf     : UITextField!
    var loginBtn : UIButton!
    var regisBtn : UIButton!
    var userLb   : UILabel!
        
        
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "登录界面"
        initBtn()
        // Do any additional setup after loading the view.
    }
    func initBtn() {
        //username
        let rect_u = CGRect(x: 70, y: 120, width: 80, height: 40 )
        userLb = UILabel(frame: rect_u)
        userLb.text = "用户名";
        userLb.font = UIFont.systemFont(ofSize: 20)
        
        //password
        pwLb = UILabel(frame: CGRect(x: 70, y: 180, width: 80, height: 40))
        pwLb.text = "密码";
        pwLb.font = UIFont.systemFont(ofSize: 20)
        self.view.addSubview(pwLb)
        //username textfield
        userTf = UITextField(frame: CGRect(x: 190, y: 120, width: 140, height: 40))
        userTf.font = UIFont.systemFont(ofSize: 20)
        userTf.textAlignment = NSTextAlignment.left
        userTf.borderStyle = UITextField.BorderStyle.roundedRect
        userTf.text = "guo";
        
        //pw textfield
        pwTf = UITextField(frame: CGRect(x: 190, y: 180, width: 140, height: 40))
        pwTf.font = UIFont.systemFont(ofSize: 20)
        pwTf.textAlignment = NSTextAlignment.left
        pwTf.borderStyle = UITextField.BorderStyle.roundedRect//特殊
        pwTf.isSecureTextEntry = true
        pwTf.text = "494919"
      
        //登陆和注册button
        loginBtn = UIButton(type: .roundedRect)//特殊
        loginBtn.frame = CGRect(x: 90, y: 240, width: 60, height: 30);
        loginBtn.backgroundColor = UIColor.yellow
        loginBtn.setTitle("登陆" , for: .normal) //特殊
        loginBtn.titleLabel?.adjustsFontSizeToFitWidth = true
        loginBtn.addTarget(self, action: #selector(pwCheck), for: .touchUpInside)//special
        
        regisBtn = UIButton(type: .roundedRect)
        regisBtn.frame =  CGRect(x: 220, y: 240, width: 60, height: 30)
        regisBtn.backgroundColor = UIColor.yellow
        regisBtn.setTitle("注册", for:.normal)
        regisBtn.titleLabel?.adjustsFontSizeToFitWidth = true
        regisBtn.addTarget(self, action: #selector(regisClick), for: .touchUpInside)
        // Do any additional setup after loading the view, typically from a nib.
       
        self.view.addSubview(userLb)
        self.view.addSubview(userTf)
        self.view.addSubview(pwTf)
        self.view.addSubview(loginBtn)
        self.view.addSubview(regisBtn)
    }
    

    @objc func pwCheck() {
//        let username:String? = userTf.text
//        let password:String? = pwTf.text
        //_userdata = [AVObject objectWithClassName:@"_User"];
       // _ = LCUser.logIn(username: username ?? "guo", password: password ?? "123") { result in
//            switch result {
//            case .success(object: let user):
//                print(user.username?.value as Any)
                let mainBoard = UIStoryboard(name: "MainInterfaceView", bundle: .main)
                let mainView = mainBoard.instantiateViewController(identifier: "MainInterfaceView")
                self.navigationController?.pushViewController(mainView, animated: true)
                //self.present(webView, animated: true, completion: nil)
//            case .failure(error: let error):
//                print(error)
//            }
        //}
        /* plist数据取得并验证
        NSString *plistPath = [[NSBundle mainBundle] pathForResource:@"userData" ofType:@"plist"];
        NSMutableDictionary *dic = [[NSMutableDictionary alloc]initWithContentsOfFile:plistPath];
        NSString *tmpUserName = [dic objectForKey: @"userName"];
        NSString *tmpPW = [dic objectForKey: @"userPw"];
         
        if ([_userName isEqualToString: self.userdata[@"username"]]){
            if ([_passWord isEqualToString: self.userdata[@"password"]]){
                NSLog(@"登录成功");
                WebViewController *webView = [[WebViewController alloc] init];
                // [self presentViewController:webView animated:YES completion:nil]; 这种方法不加在navigator
                [self.navigationController pushViewController:webView animated:YES];
            }else{
                NSLog(@"账号或密码错误");
            }
        }else{
            NSLog(@"账号或密码错误");
        }
         */
    }

    @objc func regisClick (){
        
        //SignUpViewController *registVC = [[SignUpViewController alloc] init];
        //[self.navigationController pushViewController:registVC animated:YES];
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */


}
