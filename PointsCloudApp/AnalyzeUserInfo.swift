//
//  AnalyzeUserInfo.swift 识别手机号
//  PointsCloudApp
//
//  Created by Guo on 2020/12/03.
//  Copyright © 2020 Guo. All rights reserved.
//

import Foundation

class AnalyzeUserInfo: NSObject{
    var numArr : [Character]=Array()
    var telStr : String = ""
    var count  : Int = 0
    func analyzeStr(userInfoStr: String) -> String {
        for char in userInfoStr {
            let scan = Scanner(string: String(char))
            var val:Int = 0
            if(scan.scanInt(&val) && scan.isAtEnd){
                numArr.append(char)
                count += 1
            }else if (count > 0 && count < 11 ){
                //不是手机号
                count = 0
                numArr.removeAll()
            }
            if numArr.count == 11 {
                break
            }
        }
        for i in 0..<numArr.count {
            telStr += String(numArr[i])
        }
        return telStr
    }
}
